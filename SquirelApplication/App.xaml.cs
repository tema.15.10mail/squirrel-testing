﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using MugenMvvmToolkit;
using MugenMvvmToolkit.Interfaces;
using MugenMvvmToolkit.Interfaces.Models;
using MugenMvvmToolkit.Models;
using MugenMvvmToolkit.WPF.Infrastructure;
using Serilog;
using Serilog.Formatting.Json;
using SquirelApplication.ViewModels;
using Squirrel;

namespace SquirelApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public App()
        {
            var builder = new ContainerBuilder();

            new Bootstrapper<MvvmApp>(this, new AutofacContainer(builder));
        }
        
    }

    public class MvvmApp : MvvmApplication
    {
        #region Methods

        public override Type GetStartViewModelType()
        {
            return typeof(MainViewModel);
        }

        private void Configuration()
        {
            Log.Logger =
                new LoggerConfiguration()
                .WriteTo.RollingFile(new JsonFormatter(), "logs\\myApp-{Date}.log")
                .CreateLogger();

            Application.Current.DispatcherUnhandledException += (sender, args) =>
            {
                Log.Logger.Error(args.Exception, "fatal error");
            };
        }

        protected override void OnInitialize(IList<Assembly> assemblies)
        {
            base.OnInitialize(assemblies);
            Configuration();
            Log.Logger.Information("appStart");
            Task.Run(UpdateTask);

            Log.Logger.Information("after update check");
        }

        private async Task UpdateTask()
        {
            try
            {
                using (var mgr = new UpdateManager(ConfigurationManager.AppSettings["SquirellUpdatePath"]))
                {

                    var v = mgr.CurrentlyInstalledVersion();
                    Log.Logger.Information($"currentVersion {v.Version}");
                    await mgr.UpdateApp();
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "error on update");
            }
        }

        #endregion
    }
}
