﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MugenMvvmToolkit.ViewModels;

namespace SquirelApplication.ViewModels
{
    public class MainViewModel:ValidatableViewModel
    {
        public MainViewModel()
        {
            AssemblyVersion =
                Assembly.GetAssembly(this.GetType())
                    .GetCustomAttributes<AssemblyFileVersionAttribute>()
                    .Single()
                    .Version;
        }

        public string AssemblyVersion { get; set; }
    }
}
