#load "./.build/tools.cake"

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");


Task("Restore-NuGet-Packages")    
    .Does(() =>
{
    NuGetRestore("./SquirelApplication.sln");
});

Task("Build")
  .IsDependentOn("Restore-NuGet-Packages")
  .Does(() =>
{
  MSBuild("./SquirelApplication.sln",settings =>
        settings.SetConfiguration(configuration));
});

Task("Tesing")
	.IsDependentOn("Build")
	.Does(()=>
	{
		XUnit2("./**/bin/"+configuration+"/*Test.dll");	
	});

Task("CreateDesktopRelease")
	.IsDependentOn("Build")
	.Does(() => {
		NuGetPack("SquirelApplication/SquirelApplication.nuspec", new NuGetPackSettings(){Version = "1.2.0"});
        Squirrel("TestSqApplication.1.2.0.nupkg");
    });
	
Task("Default")
	.IsDependentOn("Tesing")
	.IsDependentOn("CreateDesktopRelease");

RunTarget(target);